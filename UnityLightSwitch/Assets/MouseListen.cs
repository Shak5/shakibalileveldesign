﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseListen : MonoBehaviour
{
    public bool mouseCursoron;
    public bool mouseClicked;

    void OnMouseDown()
    {
        mouseClicked = true;   
    }

    void OnMouseUp()
    {
        mouseClicked = false; 
    }

    void OnMouseOver()
    {
        if (mouseCursoron == false)
        {
            mouseCursoron = true;
        }
    }

    void OnMouseExit()
    {
        mouseCursoron = false; 
    }


}
