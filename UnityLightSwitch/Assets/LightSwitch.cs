﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightSwitch : MonoBehaviour
{
    public TriggerListener trigger;
    public Light spotLight;
    public AudioSource audioSource;
    public Image cursorImage;
    public Animation anim;
	// Use this for initialization
	void Start ()
    {
        cursorImage.enabled = false;
        audioSource = GetComponent<AudioSource>();
        anim = GetComponent<Animation>();
	}
	
	// Update is called once per frame
	void OnMouseOver ()
    {
        if (trigger.playerEntered == true)
        {
            if (cursorImage.enabled != true)
            {
                cursorImage.enabled = true;
            }
           
        }
        else
        {
            cursorImage.enabled = false;
        }
    }
    void OnMouseExit()
    {
       
        if (cursorImage.enabled == true)
        {
            cursorImage.enabled = false;
        }
 
    }
    void OnMouseDown()
    {
        if (trigger.playerEntered == true)
        {
            audioSource.Play();

            anim.Stop();
            anim.Play();

            Debug.Log("Swtich Pressed");
            if (spotLight.intensity > 0f)
            {
                spotLight.intensity = 0f;
            }
            else
            {
                spotLight.intensity = 1.95f;
            }

        }
        
    }

}
