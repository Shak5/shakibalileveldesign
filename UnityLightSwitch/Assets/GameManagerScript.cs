﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManagerScript : MonoBehaviour
{
    public Text narrativeText;

	// Use this for initialization
	void Start ()
    {
        Invoke("IntroText1", 3);
	}
	
	
	void IntroText1()
    {
        narrativeText.text = "In a Hotel?";
        Invoke("IntroText2", 3);
    }

    void IntroText2()
    {
        narrativeText.text = "Why am I here?";
        Invoke("IntroText3", 3);
    }

    void IntroText3()
    {
        narrativeText.text = "What's going on here?";
        Invoke("BlankText", 3);
    }

    void BlankText()
    {
        narrativeText.text = null;
    }
}
